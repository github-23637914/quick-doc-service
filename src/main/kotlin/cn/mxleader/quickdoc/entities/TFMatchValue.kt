package cn.mxleader.quickdoc.entities

data class TFMatchValue(val label: String, var probability: Float)